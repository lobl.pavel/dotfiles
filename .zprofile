#
# ~/.zprofile
#

# be sure .pathrc is not writabe by others
path_set=~/.pathrc
for i in `cat $path_set`; do
    perms=`stat -Lc %a $path_set`
    if [[ $((perms & 0010)) != 0 || $((perms & 0001)) != 0 ]];then
        echo "$path_set is writable by others or group!"
        break
    fi
    export PATH=${PATH}:$i
done

# sync local mail
#offlineimap -l '.offlineimap/log' > /dev/null &

# enable dumps
ulimit -c unlimited

# startx
if [[ -z $DISPLAY && `tty` == "/dev/tty1" ]]; then
	exec startx 2> .startx.log
fi
