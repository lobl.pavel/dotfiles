

set ts=4
set modelines=5
set number
set hlsearch
set modeline
set autowrite
set relativenumber
set expandtab
set laststatus=2

colorscheme peachpuff

if (!empty(maparg('\bug')))
	unmap <Leader>bug
endif

syntax on

filetype on
filetype plugin on

au BufRead,BufNewFile *.lp set filetype=lobl

highlight Pmenu ctermbg=238
highlight LineNr ctermfg=8
highlight CursorLineNr ctermfg=10
highlight Search ctermfg=8
highlight Search ctermbg=15
highlight Special ctermfg=3
highlight Keyword ctermfg=9

" Enable syntax highlighting for LLVM files. To use, copy
" utils/vim/llvm.vim to ~/.vim/syntax .
augroup filetype
  au! BufRead,BufNewFile *.ll     set filetype=llvm
augroup END

" Enable syntax highlighting for tablegen files. To use, copy
" utils/vim/tablegen.vim to ~/.vim/syntax .
augroup filetype
  au! BufRead,BufNewFile *.td     set filetype=tablegen
augroup END

" let g:solarized_termcolors=256
"let g:NERDTreeWinPos = "right"

let g:clang_auto = 1
let g:clang_c_options = '-std=gnu11'
let g:clang_cpp_options = '-std=c++11 -stdlib=libc++'

" bindings
nmap ,f :FufFileWithCurrentBufferDir<CR>
nmap ,b :FufBuffer<CR>
nmap ,t :FufTaggedFile<CR>
nmap ,g :!rgrep

noremap <Leader>h :set norelativenumber! nonumber!<cr>
" noremap <Leader>b :Bs
noremap <Leader>e :edit<space>
noremap <Leader>w :w<cr>
noremap <Leader>l :ls<cr>
noremap <Leader>n :NERDTreeToggle<cr>
noremap <Leader>r :so ~/.vimrc<cr>
noremap <Leader>1 :bp<cr>
noremap <Leader>2 :bn<cr>
noremap <Leader>t :TagbarToggle<cr>
noremap <Leader>m :!clear && make<cr>
noremap <Leader>M :!clear && make -B<cr>

noremap <c-l> :nohls<cr><c-l>

noremap <up> <nop>
noremap <down> <nop>
noremap <left> <nop>
noremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
noremap <PageUp> <nop>
noremap <PageDown> <nop>
inoremap <PageUp> <nop>
inoremap <PageDown> <nop>
noremap <Home> <nop>
noremap <End> <nop>
inoremap <Home> <nop>
inoremap <End> <nop>

" use C highlighting for shaders
au BufRead,BufNewFile *.vp set syntax=c
au BufRead,BufNewFile *.fp set syntax=c
au BufRead,BufNewFile *.ned set syntax=cpp

" use lisp highliting for machine description (GCC)
au BufRead,BufNewFile *.md set syntax=lisp

" extra whitespace highliting
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
autocmd InsertLeave * match ExtraWhitespace /\s\+$/
autocmd BufWinLeave * call clearmatches()
